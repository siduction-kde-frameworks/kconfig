Source: kconfig
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake (>= 2.8.12),
               debhelper (>= 9),
               extra-cmake-modules (>= 5.11.0~),
               pkg-kde-tools (>= 0.15.15ubuntu1~),
               qtbase5-dev (>= 5.4),
               qttools5-dev (>= 5.4),
               qttools5-dev-tools (>= 5.4)
Standards-Version: 3.9.6
XS-Testsuite: autopkgtest
Homepage: https://projects.kde.org/projects/frameworks/kconfig
Vcs-Browser: http://anonscm.debian.org/cgit/pkg-kde/frameworks/kconfig.git
Vcs-Git: git://anonscm.debian.org/pkg-kde/frameworks/kconfig.git

Package: libkf5config-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libkf5config-bin (= ${binary:Version}),
         libkf5config-bin-dev (= ${binary:Version}),
         libkf5configcore5 (= ${binary:Version}),
         libkf5configgui5 (= ${binary:Version}),
         qtbase5-dev (>= 5.4),
         ${misc:Depends},
         ${shlibs:Depends}
Description: configuration settings framework for Qt
 KConfig provides an advanced configuration system. It is made of two
 parts: KConfigCore and KConfigGui.
 .
 KConfigCore provides access to the configuration files themselves. It
 features:
 .
  - centralized definition: define your configuration in an XML file
  and use `kconfig_compiler` to generate classes to read and write
  configuration entries.
  - lock-down (kiosk) support.
 .
 KConfigGui provides a way to hook widgets to the configuration so
 that they are automatically initialized from the configuration and
 automatically propagate their changes to their respective
 configuration files.
 .
 This package is part of KDE Frameworks 5.
 .
 This package contains the development files.

Package: libkf5config-bin-dev
Section: libdevel
Architecture: any
Multi-Arch: foreign
Breaks: libkf5config-dev (<< 5.10+git20150714)
Replaces: libkf5config-dev (<< 5.10+git20150714)
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: configuration settings framework for Qt -- binary package
 KConfig provides an advanced configuration system. It is made of two
 parts: KConfigCore and KConfigGui.
 .
 KConfigCore provides access to the configuration files themselves. It
 features:
 .
  - centralized definition: define your configuration in an XML file
  and use `kconfig_compiler` to generate classes to read and write
  configuration entries.
  - lock-down (kiosk) support.
 .
 KConfigGui provides a way to hook widgets to the configuration so
 that they are automatically initialized from the configuration and
 automatically propagate their changes to their respective
 configuration files.
 .
 This package is part of KDE Frameworks 5.
 .
 This package contains the binary files for the libkf5config-dev package.

Package: libkf5configgui5
Architecture: any
Multi-Arch: same
Depends: libkf5config-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: libkf5config-bin (= ${binary:Version})
Description: configuration settings framework for Qt
 KConfig provides an advanced configuration system. It is made of two
 parts: KConfigCore and KConfigGui.
 .
 KConfigGui provides a way to hook widgets to the configuration so
 that they are automatically initialized from the configuration and
 automatically propagate their changes to their respective
 configuration files.
 .
 This package is part of KDE Frameworks 5.
 .
 This package contains KConfigGui.

Package: libkf5configcore5
Architecture: any
Multi-Arch: same
Depends: libkf5config-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: libkf5config-bin (= ${binary:Version})
Description: configuration settings framework for Qt
 KConfig provides an advanced configuration system. It is made of two
 parts: KConfigCore and KConfigGui.
 .
 KConfigCore provides access to the configuration files themselves. It
 features:
 .
  - centralized definition: define your configuration in an XML file
  and use `kconfig_compiler` to generate classes to read and write
  configuration entries.
  - lock-down (kiosk) support.
 .
 This package is part of KDE Frameworks 5.
 .
 This package contains KConfigCore.

Package: libkf5config-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: configuration settings framework for Qt
 KConfig provides an advanced configuration system. It is made of two
 parts: KConfigCore and KConfigGui.
 .
 KConfigCore provides access to the configuration files themselves. It
 features:
 .
  - centralized definition: define your configuration in an XML file
  and use `kconfig_compiler` to generate classes to read and write
  configuration entries.
  - lock-down (kiosk) support.
 .
 KConfigGui provides a way to hook widgets to the configuration so
 that they are automatically initialized from the configuration and
 automatically propagate their changes to their respective
 configuration files.
 .
 This package is part of KDE Frameworks 5.
 .
 This package contains the translations.

Package: libkf5config-bin
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: libkf5configcore5 (<= 4.100.0-0ubuntu1~ubuntu14.10~ppa1)
Replaces: libkf5configcore5 (<= 4.100.0-0ubuntu1~ubuntu14.10~ppa1)
Description: configuration settings framework for Qt
 KConfig provides an advanced configuration system. It is made of two
 parts: KConfigCore and KConfigGui.
 .
 KConfigCore provides access to the configuration files themselves. It
 features:
 .
  - centralized definition: define your configuration in an XML file
  and use `kconfig_compiler` to generate classes to read and write
  configuration entries.
  - lock-down (kiosk) support.
 .
 KConfigGui provides a way to hook widgets to the configuration so
 that they are automatically initialized from the configuration and
 automatically propagate their changes to their respective
 configuration files.
 .
 This package is part of KDE Frameworks 5.
 .
 This package contains runtime files for KConfig.

Package: libkf5config5-dbg
Section: debug
Priority: extra
Architecture: any
Depends: libkf5configcore5 (= ${binary:Version}),
         libkf5configgui5 (= ${binary:Version}),
         ${misc:Depends}
Description: configuration settings framework for Qt
 KConfig provides an advanced configuration system. It is made of two
 parts: KConfigCore and KConfigGui.
 .
 KConfigCore provides access to the configuration files themselves. It
 features:
 .
  - centralized definition: define your configuration in an XML file
  and use `kconfig_compiler` to generate classes to read and write
  configuration entries.
  - lock-down (kiosk) support.
 .
 KConfigGui provides a way to hook widgets to the configuration so
 that they are automatically initialized from the configuration and
 automatically propagate their changes to their respective
 configuration files.
 .
 This package is part of KDE Frameworks 5.
 .
 This package contains the debugging files.
